disp('Running... Please wait...');
close all;
hold on;

% Clear all workspace variable and load new data
clear;
load('data.mat');

% Define index of data
IDX_MIN = 6;
IDX_PTS = 7;
IDX_ID = 1;

% Player to proof
% PLAYER_ID = 'HERMSCL01';
PLAYER_ID = 'BLACKCH01';

% Plot minutes vs points
for i = 1 : length(PRSC)
    minutes = str2double(PRSC(i, IDX_MIN));
    pts = str2double(PRSC(i, IDX_PTS));
    
    if strcmp(PRSC(i, IDX_ID), PLAYER_ID)
        plot(minutes, pts, 'rx');
    end
    
    plot(minutes, pts, 'r.');
end

% Put label to graph
title('Minutes Played vs Points - POC');
xlabel('Minutes Played');
ylabel('Points');