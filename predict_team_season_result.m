% Prerequisites
disp('Running... Please wait...');
close all;
hold on;
axis([1940 2010 0 1]);

% Clear all workspace variable and load new data
clear;
load('data.mat');

% Define index of data
IDX_TID = 1;
IDX_YEAR = 2;
IDX_WIN = 35;
IDX_LOSS = 36;

% Define index of team, for labelling
IDX_TLOCATION = 2;
IDX_TNAME = 3;

% Evaluate team, by using Team ID
% Example of possible values:
% BOS
% SAS
% CLE
% CHI
% DET
% SAC
% UTA
% DAL
% HOU
% LAC

% Prompt to user, which team to analyze
prompt = sprintf('Which team you want to analyze?\n1. Boston Celtics\n2. San Antonio Spurs\n3. Cleveland Cavaliers\n4. Chicago Bulls\n5. Detroit Pistons\n6. Sacramento Kings\n7. Utah Jazz\n8. Dallas Mavericks\n9. Houston Rockets\n10. Los Angeles Clippers\nYour Choice: ');
choice = input(prompt);

% Set team to analyze
switch choice
    case 1
        TEAM1 = 'BOS';
    case 2
        TEAM1 = 'SAS';
    case 3
        TEAM1 = 'CLE';
    case 4
        TEAM1 = 'CHI';
    case 5
        TEAM1 = 'DET';
    case 6
        TEAM1 = 'SAC';
    case 7
        TEAM1 = 'UTA';
    case 8
        TEAM1 = 'DAL';
    case 9
        TEAM1 = 'HOU';
    case 10
        TEAM1 = 'LAC';
    otherwise
        TEAM1 = 'BOS';
end

% Get parameter to calculate productivity
win = str2double(TS(:, IDX_WIN));
loss = str2double(TS(:, IDX_LOSS));

% Calculate productivity (win ratio), get team ID
for i = 1 : length(TS)
    gp = win(i) + loss(i);
    win_ratio = win(i) / gp;
    
    if gp ~= 0
        prod(i) = win_ratio;
    else
        prod(i) = 0;
    end
    
    tid(i) = TS(i, IDX_TID);
    year(i) = TS(i, IDX_YEAR);
end

% Find index
tmp = strfind(tid, TEAM1);
idx = find(not(cellfun('isempty', tmp)));

% Create vector to be plotted
for i = 1 : length(idx)
    plot_yr(i) = str2double(year(idx(i)));
    plot_prod(i) = prod(idx(i));
end

% Create neural network, with n hidden network
net = feedforwardnet(15);

% Train neural network, data only from 1 to n
% n = 50;
% net = train(net, plot_yr(1:n), plot_prod(1:n));

% Train neural network, whole data
net = train(net, plot_yr, plot_prod);

% Find value of y
y = net(plot_yr);

% Regression
[r, m, b] = regression(plot_prod, y);

% Plot the vector, and learning result
plot(plot_yr, plot_prod, 'r');
plot(plot_yr, y, 'b--');

% Print win ratio value if needed
% Uncomment if needed
% for i = 1 : length(plot_prod)
%     text(plot_yr(i), plot_prod(i), num2str(plot_prod(i)));
%     text(plot_yr(i), y(i), num2str(y(i)));
% end

% Find data to label graph
tmp = strfind(T(:,1), TEAM1);
idx = find(not(cellfun('isempty', tmp)));
tlocation = T(idx, IDX_TLOCATION);
tname = T(idx, IDX_TNAME);
yr_begin = num2str(plot_yr(1));
yr_end = num2str(plot_yr(length(plot_yr)));

% Put label on the graph
msg = sprintf('%s\n%s %s, %s-%s', 'Team Performance', tlocation{1}, tname{1}, yr_begin, yr_end);
title(msg);
xlabel('Year');
ylabel('Win Ratio');

% Display progress
disp('Done...');