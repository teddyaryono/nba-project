clc;
prompt = sprintf('Menu:\n1. Data Preparation - Generate dot mat\n2. Find outstanding player performace, by identifying the outlier\n3. Find outstanding coach performace, by identifying the outlier\n4. Predict season result for each team\nYour Choice: ');
x = input(prompt);

switch x
    case 1
        % Data Preparation
        run('generate_dot_mat');
    case 2
        % Find outlier player performance
        % 1. Most outstanding player during carrer
        % 2. Most do-not-impress player
        run('outlier_player_performance');
    case 3
        % Find outlier player performance
        % 1. Most outstanding coach during carrer
        % 2. Most do-not-impress coach
        run('outlier_coach_performance');
    case 4
        % Plot team season result prediction, based on previous season
        run('predict_team_season_result');
    otherwise
        run('predict_team_season_result');
end