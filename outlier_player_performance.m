% Prerequisites
disp('Running... Please wait...');
close all;
hold on;

% Clear all workspace variable and load new data
clear;
load('data.mat');

% Define index of data
IDX_MINUTES = 6;
IDX_PTS = 7;
IDX_FNAME = 2;
IDX_LNAME = 3;

% Get minutes and points to calculate productivity
minutes = str2double(PRSC(:, IDX_MINUTES));
pts = str2double(PRSC(:, IDX_PTS));

% Calculate productivity, get firstname and lastname
for i = 1 : length(PRSC)
    if minutes(i) ~= 0
        prod(i) = pts(i) / minutes(i);
    else
        prod(i) = 0;
    end
    
    fname(i) = PRSC(i, IDX_FNAME);
    lname(i) = PRSC(i, IDX_LNAME);
end

% Calculate Q1, Q2 and Q3
Q(2) = median(prod);
Q(1) = median( prod( find(prod < Q(2)) ) );
Q(3) = median( prod( find(prod > Q(2)) ) );

% Calculate interquartil
IQR = Q(3) - Q(1);

% Find lower outliers, outer fence = 3*IQR
iy = find(prod < Q(1) - 3*IQR);
if length(iy) > 0,
    outliersQ1 = prod(iy);
else
    outliersQ1 = [];
end

% Find upper outliers, outer fence = 3*IQR
iy = find(prod > Q(1) + 3*IQR);
if length(iy) > 0,
    outliersQ3 = prod(iy);
else
    outliersQ3 = [];
end

% Determine best and worst player performance
best = max(outliersQ3);
worst = min(outliersQ1);

% Plotting the graph
for i=1:length(prod)
    
    % Plot mark on upper outliers
    if ismember(prod(i), outliersQ3),
        plot(i, prod(i), 'rx');
    end
    
    % Plot mark on lower outliers
    if ismember(prod(i), outliersQ1),
        plot(i, prod(i), 'rx');
    end
    
    % Plot the best performance player
    if prod(i) == best
        plot(i, prod(i), 'ko');
        
        msg = sprintf('\t%s\n%s %s\n%s: %s', 'Most Outstanding Player', fname{i}, lname{i}, 'Productivity', num2str(prod(i)));
        text(i, prod(i), msg);
    end
    
    % Plot the worst performance player
    if prod(i) == worst
        plot(i, prod(i), 'ko');
        text(i, prod(i), '  Worst Player');
    end
    
    % Plot productivity score of players
    plot(i, prod(i), 'r.');
end

% Put label on the graph
title('Player Regular Season Performance - Whole Career');
xlabel('Players');
ylabel('Productivity (Points/minutes)');

% Create boxplot, only for supplement
figure;
boxplot(prod);
title('Player Regular Season Performance - Whole Career');
ylabel('Productivity (Points/minutes)');

% Display progress
disp('Done...');