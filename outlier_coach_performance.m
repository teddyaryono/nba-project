% Prerequisites
disp('Running... Please wait...');
close all;
hold on;
axis([0 300 -1 2]);

% Clear all workspace variable and load new data
clear;
load('data.mat');

% Define index of data
IDX_SEASON_WIN = 4;
IDX_SEASON_LOSS = 5;
IDX_PLAYOFF_WIN = 6;
IDX_PLAYOFF_LOSS = 7;
IDX_FNAME = 2;
IDX_LNAME = 3;

% Get parameter to calculate productivity
swin = str2double(CC(:, IDX_SEASON_WIN));
sloss = str2double(CC(:, IDX_SEASON_LOSS));
pwin = str2double(CC(:, IDX_PLAYOFF_WIN));
ploss = str2double(CC(:, IDX_PLAYOFF_LOSS));

% Calculate productivity (win ratio), get firstname and lastname
for i = 1 : length(CC)
    
    win = swin(i) + pwin(i);
    loss = sloss(i) + ploss(i);
    gp = win + loss;
    
    if gp ~= 0
        prod(i) = win / gp;
    else
        prod(i) = 0;
    end
    
    fname(i) = CC(i, IDX_FNAME);
    lname(i) = CC(i, IDX_LNAME);
end

% Calculate Q1, Q2 and Q3
Q(2) = median(prod);
Q(1) = median( prod( find(prod < Q(2)) ) );
Q(3) = median( prod( find(prod > Q(2)) ) );

% Calculate interquartil
IQR = Q(3) - Q(1);

% Find lower outliers, outer fence = 3*IQR
iy = find(prod < Q(1) - 3*IQR);
if length(iy) > 0,
    outliersQ1 = prod(iy);
else
    outliersQ1 = [];
end

% Find upper outliers, outer fence = 3*IQR
iy = find(prod > Q(1) + 3*IQR);
if length(iy) > 0,
    outliersQ3 = prod(iy);
else
    outliersQ3 = [];
end

% Determine best and worst coach performance
best = max(outliersQ3);
worst = min(outliersQ1);

% Plotting the graph
for i=1:length(prod)
    
    % Plot mark on upper outliers
    if ismember(prod(i), outliersQ3),
        plot(i, prod(i), 'rx');
    end
    
    % Plot mark on lower outliers
    if ismember(prod(i), outliersQ1),
        plot(i, prod(i), 'rx');
    end
    
    % Plot the best performance coach
    if prod(i) == best
        plot(i, prod(i), 'ko');
        
        msg = sprintf('\t%s\n%s %s\n%s: %s', 'Most Outstanding Coach', fname{i}, lname{i}, 'Win Ratio', num2str(prod(i)));
        text(i, prod(i), msg);
    end
    
    % Plot the worst performance coach
    if prod(i) == worst
        plot(i, prod(i), 'ko');
        text(i, prod(i), '  Worst Coach');
    end
    
    % Plot productivity score of players
    plot(i, prod(i), 'r.');
end

% Put label on the graph
title('Coach Performance - Whole Career');
xlabel('Coach');
ylabel('Productivity (Win Ratio)');

% Create boxplot, only for supplement
figure;
boxplot(prod);
title('Coach Performance - Whole Career');
ylabel('Productivity (Win Ratio)');

% Display progress
disp('Done...');