function lineArray = read_mixed_csv(fileName,delimiter)
    
    % Open the file
    fid = fopen(fileName,'r');   
    
    % Preallocate a cell array (ideally slightly larger than is needed)
    lineArray = cell(100,1);     
    
    % Index of cell to place the next line in
    lineIndex = 1;               
    
    % Read the first line from the file
    nextLine = fgetl(fid);       
    
    % Loop while not at the end of the file
    while ~isequal(nextLine,-1)           
        % Add the line to the cell array
        lineArray{lineIndex} = nextLine; 
        
        % Increment the line index
        lineIndex = lineIndex+1;
        
        % Display progress every 1000 records
        if mod(lineIndex, 1000) == 0
            disp('1000 records read...');
        end
        
        % Read the next line from the file
        nextLine = fgetl(fid);      
    end
    
    % Close the file
    fclose(fid);                           
    
    % Remove empty cells, if needed
    lineArray = lineArray(1:lineIndex-1);  
    
    % Loop over lines
    for iLine = 1:lineIndex-1              
        % Read strings
        lineData = textscan(lineArray{iLine},'%s',...  
                            'Delimiter',delimiter);
                        
        % Remove cell encapsulation
        lineData = lineData{1};              
        
        % Account for when the line
        if strcmp(lineArray{iLine}(end),delimiter)  
            % ends with a delimiter
            lineData{end+1} = '';                     
        end
        
        % Overwrite line data
        lineArray(iLine,1:numel(lineData)) = lineData;  
    end
    
end