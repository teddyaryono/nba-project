% DATA PREPARATION
% Clear all variables
clear;

% Prompt user to configure environment
prompt = sprintf('Are you in development?\n1. Yes\n2. No\nYour Choice: ');
x = input(prompt);

% Determine if project is on development or production
switch x
    case 1
        is_development = true;
    case 2
        is_development = false;
    otherwise
        is_development = true;
end


% Load data from CSV
[ T, TS, P, PA, PP, PPC, PRS, PRSC, CC, CS, D ] = configure_environment( is_development );

% Create struct as a supplement only
% Un-comment if needed
% [ t, ts, p, pa, pp, ppc, prs, prsc, cc, cs, d ] = create_struct(T, TS, P, PA, PP, PPC, PRS, PRSC, CC, CS, D);

% Clear un-needed variables
clear i;
clear is_development;
clear prompt;
clear x;

% Save data as data.mat
disp('Saving data to .mat file...');
save('data.mat');
disp('Done...');