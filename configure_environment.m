function [ A, B, C, D, E, F, G, H, I, J, K ] = configure_environment( is_development )
    
    % If parameter is missing, then load original data
    if nargin < 1
        is_development = false;
    end

    % For development purpose, load small amount of data
    if is_development
        A = read_mixed_csv('data-dev/teams.txt', ',');
        B = read_mixed_csv('data-dev/team_season.txt', ',');
        C = read_mixed_csv('data-dev/players.txt', ',');
        D = read_mixed_csv('data-dev/player_allstar.txt', ',');
        E = read_mixed_csv('data-dev/player_playoffs.txt', ',');
        F = read_mixed_csv('data-dev/player_playoffs_career.txt', ',');
        G = read_mixed_csv('data-dev/player_regular_season.txt', ',');
        H = read_mixed_csv('data-dev/player_regular_season_career.txt', ',');
        I = read_mixed_csv('data-dev/coaches_career.txt', ',');
        J = read_mixed_csv('data-dev/coaches_season.txt', ',');
        K = read_mixed_csv('data-dev/draft.txt', ',');
    else
        % Otherwise, load the original ones
        A = read_mixed_csv('data-prod/teams.txt', ',');
        B = read_mixed_csv('data-prod/team_season.txt', ',');
        C = read_mixed_csv('data-prod/players.txt', ',');
        D = read_mixed_csv('data-prod/player_allstar.txt', ',');
        E = read_mixed_csv('data-prod/player_playoffs.txt', ',');
        F = read_mixed_csv('data-prod/player_playoffs_career.txt', ',');
        G = read_mixed_csv('data-prod/player_regular_season.txt', ',');
        H = read_mixed_csv('data-prod/player_regular_season_career.txt', ',');
        I = read_mixed_csv('data-prod/coaches_career.txt', ',');
        J = read_mixed_csv('data-prod/coaches_season.txt', ',');
        K = read_mixed_csv('data-prod/draft.txt', ',');
    end
    
end

