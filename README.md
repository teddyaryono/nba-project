## Function:
- `create_struct.m`
- `configure_environment.m`
- `read_mixed_csv.m`

## Aplikasi:
- Jalankan `main.m`, atau ke command window dan ketikkan `main`
- Lihat command window pada MATLAB
    - Ketik angka 1 untuk melakukan data preparation
        - Ketika muncul pertanyaan __"Are you in development"__, maka:
            - Ketik angka 1 untuk me-load hanya sebagian data (development/presentation mode)
            - Ketik angka 2 untuk me-load seluruh data (butuh waktu agak lama)
    - Ketik angka 2 untuk menjalankan file `outlier_player_performance.m`
    - Ketik angka 3 untuk menjalankan file `outlier_coach_performance.m`
    - Ketik angka 4 untuk menjalankan file `perdict_team_season_result.m`
        - ketika muncul pertanyaan __"Which team you want to analyze?"__, maka:
            - Ketikkan angka yang sesuai dengan tim yang akan dianalisa.
            - Contoh: Angka 1 untuk tim Boston Celtics, dst.
- Exit saat program sedang berjalan: `CTRL+C`

## Algoritma Pencarian Outliers:
```
- Cari median (Q2)
- Cari Q1 dan Q3
- Cari jangkauan antar quartil (Interquartile Range/IQR)
- Cari pagar luar (outer fence). Disini memakai 3 * IQR
- Upper outliers: semua angka yang berjarak 3 * IQR di atas outer fence
- Lower outliers: semua angka yang berjarak 3 * IQR di bawah outer fence
```

## Screenshots:

<center><img src="http://i.imgur.com/CVNlUyM.png" alt="screenshot" style="width:100%;" /><br/></center>
<center><img src="http://i.imgur.com/schMeFQ.png" alt="screenshot" style="width:100%;" /><br/></center>
<center><img src="http://i.imgur.com/KfidGuJ.png" alt="screenshot" style="width:100%;" /><br/></center>
<center><img src="http://i.imgur.com/XL9CmyM.png" alt="screenshot" style="width:100%;" /><br/></center>
<center><img src="http://i.imgur.com/RwblTQk.png" alt="screenshot" style="width:100%;" /><br/></center>
<center><img src="http://i.imgur.com/vxiq6HE.png" alt="screenshot" style="width:100%;" /><br/></center>
<center><img src="http://i.imgur.com/uMgIkC5.png" alt="screenshot" style="width:100%;" /><br/></center>
<center><img src="http://i.imgur.com/E1vgjnJ.png" alt="screenshot" style="width:100%;" /><br/></center>
<center><img src="http://i.imgur.com/TW2Z9Cg.png" alt="screenshot" style="width:100%;" /><br/></center>
<center><img src="http://i.imgur.com/uWkvaW6.png" alt="screenshot" style="width:100%;" /><br/></center>
<center><img src="http://i.imgur.com/Twbj8wh.png" alt="screenshot" style="width:100%;" /><br/></center>
<center><img src="http://i.imgur.com/eH5mkcJ.png" alt="screenshot" style="width:100%;" /><br/></center>